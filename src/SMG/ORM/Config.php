<?php

namespace SMG\ORM;

use Symfony\Component\Yaml\Yaml;

/**
 * Description of Config
 *
 * @author richard
 */
class Config {
    static $config;
    protected static $DBO;
    static function Parse($file)
    {
        self::$config = Yaml::parse(file_get_contents($file));
    }
    static function get($key)
    {
        return (isset(self::$config[$key])) ? self::$config[$key] : null;
    }
    static function Connect($class, $params)
    {
        switch ($class) {
            case 'PDO':
                if (!is_a(self::$DBO, 'PDO')) {
                    self::$DBO = new \PDO($params['dsn'], $params['db']['user'], $params['db']['pass'], $params['opt']);
                }
                break;
        }
        return self::$DBO;
    }
}
