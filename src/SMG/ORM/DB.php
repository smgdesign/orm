<?php
namespace SMG\ORM;

use SMG\ORM\Entity;
use SMG\ORM\Config;
/**
 * Description of DB
 *
 * @author richard
 */
class DB extends DBO {
    protected $data;
    protected static $index = 'id';
    // the definition of columns \\
    protected $cols = [];
    // the prepared set of columns \\
    protected $col = [];
    // the columns to select for the query \\
    protected $pcols = [];
    // the definition of join columns \\
    protected $joins = [];
    // the prepared set of joins \\
    protected $join = [];
    // the instance that has called this \\
    protected $Instance;
    protected $order = [];
    public function __construct(Entity &$entity) {
        $this->Instance = &$entity;
        parent::__construct();
    }
    private function GetNamespace(Entity $instance)
    {
        $class = get_class($instance);
        $lastSlashPosition = strrpos($class, '\\');
        $namespace = substr($class, 0, $lastSlashPosition);
        return $namespace;
    }
    public function SetCols($cols)
    {
        $this->cols = $cols;
    }
    public function SetJoins($joins)
    {
        $this->joins = $joins;
    }
    public function Find($id)
    {
        $joins = $this->PrepareJoins(true);
        $cols = $this->PrepareColumns(true);
        $order = $this->Instance->GetOrder();
        $item = $this->Select("SELECT $cols FROM {$this->Instance->GetEntityName()} AS `{$this->Instance->GetEntityName()}` $joins WHERE `{$this->Instance->GetEntityName()}`.`{$this->Instance->GetIndex()}`=:id $order", ['id'=>$id]);
        return $this->PrepareEntities($item);
    }
    public function FindBy($vals, $limit=0)
    {
        return $this->Search($vals, $limit);
    }
    public function FindOneBy($vals)
    {
        $result = $this->FindBy($vals, 1);
        return reset($result);
    }
    public function __get($name) {
        if (is_array($this->data)) {
            if (!isset($this->data[$name])) {
                return null;
            }
            return $this->data[$name];
        } else {
            if (!isset($this->data->{$name})) {
                return null;
            }
            return $this->data->{$name};
        }
    }
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
    private function Search($vals, $limit=0, $offset=0)
    {
        $this->order = $this->Instance->GetOrder();
        $joins = $this->PrepareJoins(true);
        $colsp = $this->PrepareColumns(true);
        $results = [];
        $prepared = [];
        foreach ($vals as $entity=>$cols) {
            $entity_cols = [];
            // default method to join multiple params \\
            if (!isset($cols['join'])) {
                $cols['join'] = 'AND';
            }
            foreach ($cols['columns'] as $col=>$val) {
                $entity_cols[] = "`$entity`.`$col`".((is_array($val)) ? $val[0] : '=').":{$entity}_{$col}";
                $prepared[$entity.'_'.$col] = (is_array($val)) ? $val[1] : $val;
            }
            array_walk($prepared, array(&$this, 'DetectTypes'));
            $results[] = '('.implode(' '.$cols['join'].' ', $entity_cols).')';
        }
        $resultsStr = implode(' AND ', $results);
        $limitStr = '';
        if ($limit > 0) {
            $limitStr = "LIMIT $offset, $limit";
        }
        if (!empty($this->order)) {
            $this->order = 'ORDER BY '.ltrim(implode(', ', $this->order), ', ');
        }
//        print_r("SELECT $colsp FROM {$this->Instance->GetEntityName()} AS `{$this->Instance->GetEntityName()}` $joins WHERE $resultsStr $limitStr {$this->order}");
        $item = $this->Select("SELECT $colsp FROM {$this->Instance->GetEntityName()} AS `{$this->Instance->GetEntityName()}` $joins WHERE $resultsStr $limitStr {$this->order}", $prepared);
        return $this->PrepareEntities($item);
    }
    public static function Index()
    {
        return self::$index;
    }
    private function PrepareColumns($implode=false)
    {
        $this->RecurseColumns($this->Instance);
        return ($implode) ? implode(', ', $this->pcols) : $this->pcols;
    }
    private function RecurseColumns(Entity $Entity)
    {
        foreach ($Entity->GetCols() as $en=>$ecols) {
            foreach ($ecols as $col) {
                $this->pcols[$en.$col] = "`$en`.`$col` AS {$en}_{$col}";
            }
        }
    }
    private function PrepareJoins($implode=false)
    {
        $joins = [];
        foreach ($this->Instance->GetJoins() as $join) {
            $this->RecurseJoins($this->Instance, $join, $joins);
        }
        return ($implode) ? implode(' ', $joins) : $joins;
    }
    private function RecurseJoins(Entity $entity, Entity $join, &$joins)
    {
        // only do this if the current entity isn't what we're looking for! \\
        if ($join->GetEntityName() !== $this->Instance->GetEntityName()) {
            if ($join->GetRelation()) {
                // this is a one to many so shit gets real! \\
                $this->PrepareMany($join, $joins);
//                $join->SetCol($join->GetEntityName(), $join->GetIndex());
            } else if (isset($entity->GetSchema()["{$join->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetIndex()}"])) {
                $joins[$join->GetEntityName()] = "LEFT JOIN {$join->GetEntityName()} AS `{$join->GetEntityName()}` ON `{$join->GetEntityName()}`.`{$join->GetIndex()}`=`{$entity->GetEntityName()}`.`{$join->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetIndex()}`";
            } else if (isset($join->GetSchema()["{$entity->GetEntityName($this->config['lcase_entity_joins'])}_{$entity->GetIndex()}"])) {
                $joins[$join->GetEntityName()] = "LEFT JOIN {$join->GetEntityName()} AS `{$join->GetEntityName()}` ON `{$join->GetEntityName()}`.`{$entity->GetEntityName($this->config['lcase_entity_joins'])}_{$entity->GetIndex()}`=`{$entity->GetEntityName()}`.`{$entity->GetIndex()}`";
            }
            // prepare the columns \\
            $this->RecurseColumns($join);
            if (!empty($join->GetOrder())) {
                $this->order[] = $join->GetOrder();
            }
            foreach ($join->GetJoins() as $sjoin) {
                $this->RecurseJoins($join, $sjoin, $joins);
            }
        }
    }
    private function PrepareMany(Entity $join, &$joins)
    {
        // the joiner table \\
        if ($join->GetRelation()->GetEntityName() !== $this->Instance->GetEntityName()) {
            $on = $join->GetOn();
            $joins[$join->GetEntityName()] = "LEFT JOIN {$join->GetEntityName()} AS `{$join->GetEntityName()}` ON `{$join->GetEntityName()}`.`{$on::EntityName($this->config['lcase_entity_joins'])}_{$on::Index()}`=`{$on::EntityName()}`.`{$on::Index()}`";
            $joins[$join->GetRelation()->GetEntityName()] = "LEFT JOIN {$join->GetRelation()->GetEntityName()} AS `{$join->GetRelation()->GetEntityName()}` ON `{$join->GetRelation()->GetEntityName()}`.`{$join->GetRelation()->GetIndex()}`=`{$join->GetEntityName()}`.`{$join->GetRelation()->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetRelation()->GetIndex()}`";
            $join->SetCol($join->GetEntityName(), $join->GetIndex())->SetCol($join->GetEntityName(), "{$on::EntityName($this->config['lcase_entity_joins'])}_{$on::Index()}")->SetCol($join->GetEntityName(), "{$join->GetRelation()->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetRelation()->GetIndex()}");
            $this->RecurseColumns($join);
            foreach ($join->GetJoins() as $sjoin) {
                $this->RecurseJoins($join, $sjoin, $joins);
            }
            $this->RecurseColumns($join->GetRelation());
            foreach ($join->GetRelation()->GetJoins() as $ojoin) {
                $this->RecurseJoins($join->GetRelation(), $ojoin, $joins);
            }
        }
    }
    private function PrepareEntities($item)
    {
        $Entities = [];
        $this->RecurseEntities($item, $this->Instance, $Entities);
        return $Entities;
    }
    private function RecurseEntities($item, Entity $entity, &$Entities)
    {
        $filled = false;
        $parent = null;
        $entity->Unique = uniqid();
        foreach ($item as $row) {
            foreach ($entity->GetCols() as $entityName=>$cols) {
                $fqEntityName = $this->GetNamespace($entity).'\\'.str_replace('_', '', $entityName);
                $EntityIndex = $entityName.'_'.$fqEntityName::Index();
                if ($entityName === $entity->GetEntityName() && !isset($Entities[$row->$EntityIndex])) {
                    if (!$filled) {
                        $Entities[$row->$EntityIndex] = &$entity;
                        $filled = true;
                    } else {
                        $Entities[$row->$EntityIndex] = new $entity();
                    }
                    $this->RecurseSubEntities($Entities[$row->$EntityIndex], $row, $parent);
                } else if ($entityName !== $entity->GetEntityName()) {
                    $SubEntity = new $fqEntityName();
                    $TgtEntityIndex = $entity->GetEntityName().'_'.$entity->GetIndex();
                    $this->AddEntityToEntity($Entities[$row->$TgtEntityIndex], $SubEntity);
                    $this->RecurseSubEntities($SubEntity, $row, $Entities[$row->$TgtEntityIndex]);
                } else {
                    $this->RecurseSubEntities($Entities[$row->$EntityIndex], $row, $parent);
                }
            }
        }
    }
    private function RecurseSubEntities(Entity &$SubEntity, $row, &$parent)
    {
        foreach ($SubEntity->GetCols() as $entityName=>$cols) {
            $fqEntityName = $this->GetNamespace($SubEntity).'\\'.str_replace('_', '', $entityName);
            if ($entityName === $SubEntity->GetEntityName()) {
                $TmpEntity = $this->AddColsToEntity($SubEntity, $cols, $row);
                if ($TmpEntity !== false) {
                    $SubEntity = $TmpEntity;
                }
                if (!is_null($parent)) {
                    $this->AddEntityToEntity($parent, $SubEntity);
                }
            } else if (class_exists($fqEntityName)) {
                $SubSubEntity = ($fqEntityName::Relation()) ? clone $SubEntity->GetJoins()[$entityName] : new $fqEntityName();
                if (!is_null($SubSubEntity) && $SubSubEntity->GetRelation()) {
                    // this is a joiner \\
                    $SubSubEntity->{$SubSubEntity->GetOn()} = $parent;
                }
                $TmpEntity = $this->AddColsToEntity($SubSubEntity, $cols, $row);
                if ($TmpEntity !== false) {
                    $SubSubEntity = $TmpEntity;
                }
                $this->AddEntityToEntity($SubEntity, $SubSubEntity);
                $this->RecurseSubEntities($SubSubEntity, $row, $SubEntity);
            }
        }
        foreach ($SubEntity->GetJoins() as $join) {
            $entityName = $join->GetEntityName();
            if ($entityName !== $SubEntity->GetEntityName()) {
//                $SubSubEntity = (is_a($SubEntity->$entityName, '\SMG\ORM\Entity')) ? $SubEntity->$entityName : new $fqEntityName();
                if (isset($SubEntity->{$join->GetEntityName()}) && !is_array($SubEntity->{$join->GetEntityName()})) {
                    $SubSubEntity = $SubEntity->{$join->GetEntityName()};
                } else {
                    $SubSubEntity = $join;
                }
                if (!is_null($SubSubEntity) && $SubSubEntity->GetRelation()) {
                    // this is a joiner \\
                    $SubSubEntity->{$SubSubEntity->GetOn()} = $SubEntity;
                    $Relation = $SubSubEntity->GetRelation();
                    if (isset($SubEntity->{$Relation->GetEntityName()}) && !is_array($SubEntity->{$Relation->GetEntityName()})) {
                        $Relation = $SubEntity->{$Relation->GetEntityName()};
                    }
                    $this->RecurseSubEntities($Relation, $row, $SubEntity);
                }
                $SubSubEntity->Unique = uniqid();
                $this->RecurseSubEntities($SubSubEntity, $row, $SubEntity);
            }
        }
    }
    private function AddColsToEntity(Entity &$entity, $cols, $vals)
    {
        $fqname = get_class($entity);
        if ($entity->GetRelation()) {
            return false;
        }
        $tmpEntity = new $fqname();
        $new = false;
        foreach ($cols as $col) {
            if ((isset($entity->$col) && $entity->$col !== $vals->{$entity->GetEntityName().'_'.$col}) || $new) {
                $tmpEntity->$col = $vals->{$entity->GetEntityName().'_'.$col};
                $new = true;
            } else {
                $entity->$col = $vals->{$entity->GetEntityName().'_'.$col};
            }
        }
        return ($new) ? $tmpEntity : false;
    }
    private function AddEntityToEntity(Entity &$Entity, Entity $SubEntity)
    {
        if (!isset($Entity->{$SubEntity->GetEntityName()})) {
            $Entity->{$SubEntity->GetEntityName()} = $SubEntity;
            return true;
        } else if (!is_array($Entity->{$SubEntity->GetEntityName()}) && $Entity->{$SubEntity->GetEntityName()}->{$SubEntity->GetIndex()} === $SubEntity->{$SubEntity->GetIndex()} || is_null($SubEntity->{$SubEntity->GetIndex()})) {
            // this means we have this entity already defined so need to ignore it \\
            return true;
        } else if (!is_array($Entity->{$SubEntity->GetEntityName()})) {
            $TmpEntity = $Entity->{$SubEntity->GetEntityName()};
            $Entity->{$SubEntity->GetEntityName()} = [$TmpEntity->{$TmpEntity->GetIndex()}=>$TmpEntity];
        }
        $Entity->Append($SubEntity->GetEntityName(), $SubEntity->{$SubEntity->GetIndex()}, $SubEntity);
        return true;
    }
    public function Save($cascade=false)
    {
        $entities = [];
        $entity_joins = [];
        foreach ($this->Instance->GetCols() as $entity=>$cols) {
            if ($entity === $this->Instance->GetEntityName()) {
                $this->DeltaChanges($this->Instance, $cols, $entities);
            } else if ($cascade) {
                // need to check sub entities and build the appropriate modifiers for them - if Save is cascading \\
                $SubEntity = $this->Instance->$entity;
                if (!is_null($SubEntity) && !is_array($SubEntity)) {
                    $this->DeltaChanges($SubEntity, $cols, $entities);
                } else if (is_array($SubEntity)) {
                    // this means it's a collection \\
                    foreach ($SubEntity as $SubCollectionEntity) {
                        $this->DeltaChanges($SubCollectionEntity, $cols, $entities);
                    }
                }
            }
        }
        if ($cascade) {
            // also include the join columns if required \\
            foreach ($this->Instance->GetJoins() as $join) {
                if ($join->GetRelation()) {
                    // this is a one to many so shit gets real! \\
                    $this->DeltaManyChanges($join, $entity_joins);
                } else if (
                        isset($this->Instance->GetSchema()["{$join->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetIndex()}"]) &&
                        (
                            !isset($this->Instance->{$join->GetEntityName()}) ||
                            (
                                !isset($this->Instance->GetExisting()[$join->GetEntityName()]) ||
                                $this->Instance->GetExisting()[$join->GetEntityName()]->{$join->GetIndex()} !== $this->Instance->{$join->GetEntityName()}->{$join->GetIndex()}
                            ) ||
                            !isset($this->Instance->{$join->GetEntityName()}->GetExisting()[$join->GetIndex()]) ||
                            (
                                $this->Instance->{$join->GetEntityName()}->{$join->GetIndex()} !== $this->Instance->{$join->GetEntityName()}->GetExisting()[$join->GetIndex()]
                            ) ||
                            (
                                is_null($this->Instance->{$this->Instance->GetIndex()})
                            ) &&
                            !is_null($this->Instance->{$join->GetEntityName()}->{$join->GetIndex()})
                        )
                ) {
                    if (!isset($this->Instance->{$join->GetEntityName()})) {
                        $fqentity = $this->GetNamespace($this->Instance).'\\'.str_replace('_', '', $join->GetEntityName());
                        $this->Instance->{$join->GetEntityName()} = new $fqentity();
                    }
                    $entities[$this->Instance->GetEntityName()]['columns']["{$join->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetIndex()}"] = $this->Instance->{$join->GetEntityName()}->{$join->GetIndex()};
                }
            }
        }
        $this->Persist($entities, $entity_joins);
    }
    private function DeltaChanges(Entity $Entity, $cols, &$entities)
    {
        foreach ($cols as $col) {
            if ((!isset($Entity->GetExisting()[$col]) || $Entity->$col !== $Entity->GetExisting()[$col]) || (is_null($Entity->{$Entity->GetIndex()})) && !is_null($Entity->$col)) {
                if (!isset($entities[$Entity->GetEntityName()])) {
                    // generic update clause assuming we will always update on the ID as we have an instance \\
                    $entities[$Entity->GetEntityName()] = ['columns'=>[], 'condition'=>[], 'entity'=>&$Entity];
                    if (!is_null($Entity->{$Entity->GetIndex()})) {
                        $entities[$Entity->GetEntityName()]['condition'][$Entity->GetIndex()] = $Entity->{$Entity->GetIndex()};
                    }
                }
                $entities[$Entity->GetEntityName()]['columns'][$col] = $Entity->$col;
            }
        }
    }
    private function DeltaManyChanges(Entity $join, &$entity_joins)
    {
        if (!$join->GetCascade()) {
            return;
        }
        // the joiner table \\
        $on = $join->GetOn();
        if (!isset($join->{$on::EntityName()})) {
            $fqentity = $this->GetNamespace($join).'\\'.str_replace('_', '', $on::EntityName());
            $join->{$on::EntityName()} = new $fqentity();
        }
        if ((!isset($join->GetExisting()[$join->GetIndex()]) || $join->{$on::EntityName()}->{$on::Index()} !== $join->{$on::EntityName()}->GetExisting()[$on::Index()]) || (is_null($join->{$join->GetIndex()})) && !is_null($join->{$on::EntityName()}->{$on::GetIndex()})) {
            if (!isset($entity_joins[$join->GetEntityName()])) {
                // generic update clause assuming we will always update on the ID as we have an instance \\
                $entity_joins[$join->GetEntityName()] = ['columns'=>[], 'condition'=>[], 'entity'=>&$join];
                if (!is_null($join->{$join->GetIndex()})) {
                    $entity_joins[$join->GetEntityName()]['condition'][$join->GetIndex()] = $join->{$join->GetIndex()};
                }
            }
            if (!isset($join->{$on::EntityName()}->{$on::Index()})) {
                $join->{$on::EntityName()}->{$on::Index()} = -1;
            }
            $entity_joins[$join->GetEntityName()]['columns']["{$on::EntityName($this->config['lcase_entity_joins'])}_{$on::Index()}"] = $join->{$on::EntityName()}->{$on::Index()};
            if (!isset($join->{$join->GetRelation()->GetEntityName()})) {
                $entity_joins[$join->GetEntityName()]['columns']["{$join->GetRelation()->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetRelation()->GetIndex()}"] = -1;
            } else {
                $entity_joins[$join->GetEntityName()]['columns']["{$join->GetRelation()->GetEntityName($this->config['lcase_entity_joins'])}_{$join->GetRelation()->GetIndex()}"] = $join->{$join->GetRelation()->GetEntityName()}->{$join->GetRelation()->GetIndex()};
            }
        }
    }
}
