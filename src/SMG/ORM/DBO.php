<?php

namespace SMG\ORM;

/**
 * Description of DBO
 *
 * @author richard
 */
class DBO {
    /**
     *
     * @var \PDO 
     */
    var $DB;
    var $config;
    public function __construct() {
        $db = Config::get('database');
        $dsn = "{$db['type']}:host={$db['host']};dbname={$db['name']};charset={$db['charset']}";
        $opt = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->DB = Config::Connect('PDO', ['dsn'=>$dsn, 'opt'=>$opt, 'db'=>$db]);
        $this->config = $db;
    }
    public function Select($query, $params)
    {
        $statement = $this->DB->prepare($query);
        $statement->execute($params);
        return $statement->fetchAll();
    }
    public function Execute($query, $params)
    {
        $stmt = $this->DB->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchColumn();
    }
    public function Persist($tables=[], $joins=[])
    {
        if (!empty($tables) || !empty($joins)) {
            $inserts = [];
            $commit = !empty($joins);
            foreach ($tables as $table=>&$data) {
                if (empty($data['condition'])) {
                    // is an insert \\
                    $cols = implode('`,`', array_keys($data['columns']));
                    $vals = implode(',:', array_keys($data['columns']));
//                    print_r("INSERT INTO `$table` (`$cols`) VALUES (:$vals)");
                    $data['prepared'] = $this->DB->prepare("INSERT INTO `$table` (`$cols`) VALUES (:$vals)");
                    array_walk($data['columns'], array(&$this, 'DetectTypes'));
                    $data['bind'] = ['vals'=>$data['columns']];
                    $data['insert'] = true;
                } else {
                    // is an update \\
                    $cols = implode('`=?,`', array_keys($data['columns']));
                    $cond = implode('`=? AND `', array_keys($data['condition']));
                    $vals = array_merge(array_values($data['columns']), array_values($data['condition']));
//                    print_r("UPDATE `$table` SET `$cols`=? WHERE `$cond`=?");
                    $data['prepared'] = $this->DB->prepare("UPDATE `$table` SET `$cols`=? WHERE `$cond`=?");
                    array_walk($vals, array(&$this, 'DetectTypes'));
                    $data['bind'] = ['vals'=>$vals];
                }
            }
            if (!empty($tables)) {
                $this->DB->beginTransaction();
                foreach ($tables as $data) {
                    // just use the prepared and bind \\
                    if (isset($data['insert'])) {
                        $inserts[] = $data;
                        continue;
                    }
                    $data['prepared']->execute($data['bind']['vals']);
                }
                $commit = $this->DB->commit();
            }
            if ($commit) {
                foreach ($inserts as $insert) {
                    $insert['prepared']->execute($insert['bind']['vals']);
                    $insert['entity']->{$insert['entity']->GetIndex()} = $this->DB->lastInsertId();
                    // find references to this ID \\
                    $this->FindJoinReferences($insert['entity'], $joins);
                }
                if (!empty($joins)) {
                    $this->DB->beginTransaction();
                    foreach ($joins as $table=>&$data) {
                        if (empty($data['condition'])) {
                            // is an insert \\
                            $cols = implode('`,`', array_keys($data['columns']));
                            $vals = implode(',:', array_keys($data['columns']));
//                            print_r("INSERT INTO `$table` (`$cols`) VALUES (:$vals)");
                            $data['prepared'] = $this->DB->prepare("INSERT INTO `$table` (`$cols`) VALUES (:$vals)");
                            array_walk($data['columns'], array(&$this, 'DetectTypes'));
                            $data['bind'] = ['vals'=>$data['columns']];
                            $data['insert'] = true;
                        } else {
                            // is an update \\
                            $cols = implode('`=?,`', array_keys($data['columns']));
                            $cond = implode('`=? AND `', array_keys($data['condition']));
                            $vals = array_merge(array_values($data['columns']), array_values($data['condition']));
//                            print_r("UPDATE `$table` SET `$cols`=? WHERE `$cond`=?");
                            $data['prepared'] = $this->DB->prepare("UPDATE `$table` SET `$cols`=? WHERE `$cond`=?");
                            array_walk($vals, array(&$this, 'DetectTypes'));
                            $data['bind'] = ['vals'=>$vals];
                        }
                    }
                    foreach ($joins as $table=>&$data) {
                        $data['prepared']->execute($data['bind']['vals']);
                    }
                    if ($this->DB->commit()) {
                        return true;
                    }
                    $this->DB->rollBack();
                    return false;
                }
                return true;
            }
            $this->DB->rollBack();
            return false;
        }
    }
    public function DetectTypes(&$item)
    {
        if (is_a($item, 'DateTime')) {
            // we need this to be in MySQL format \\
            $item = $item->format('Y-m-d H:i:s');
        }
    }
    private function FindJoinReferences(Entity $entity, &$joins)
    {
        foreach ($joins as &$join) {
            if (isset($join['columns'][$entity->GetEntityName(true).'_'.$entity->GetIndex()]) && $join['columns'][$entity->GetEntityName(true).'_'.$entity->GetIndex()] === -1) {
                // this needs redefining \\
                $join['columns'][$entity->GetEntityName(true).'_'.$entity->GetIndex()] = $entity->{$entity->GetIndex()};
            }
        }
    }
}
