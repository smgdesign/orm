<?php

namespace SMG\ORM;

use SMG\ORM\DB;

class Entity {
    /**
     *
     * @var DB 
     */
    protected $DB;
    protected $data = [];
    protected $existing = [];
    protected static $index = 'id';
    protected static $entity;
    protected static $schema;
    protected static $order;
    /**
     *
     * @var array 
     */
    protected $hidden = [];
    /**
     *
     * @var Entity 
     */
    protected static $relation;
    protected static $type = '';
    /**
     *
     * @var string 
     */
    protected static $on;
    protected static $cascade = true;
    protected $joins = [];
    protected $cols = [];
    // the Collection of all results which are instances of self \\
    protected $Collection;
    public function __construct($vals=[])
    {
        static::$on = get_called_class();
        $this->DB = new DB($this);
        $this->DB->SetJoins($this->joins);
        $this->DB->SetCols($this->cols);
        if (!empty($vals)) {
            $this->FindBy($vals);
        }
    }
    public function GetEntityName($lower=false)
    {
        return static::EntityName($lower);
    }
    public static function EntityName($lower=false)
    {
        return ($lower) ? strtolower(static::$entity) : static::$entity;
    }
    public function GetSchema()
    {
        return static::Schema();
    }
    public static function Schema()
    {
        return static::$schema;
    }
    public function GetOrder()
    {
        return static::Order();
    }
    public static function Order()
    {
        $outp = '';
        if (!empty(static::$order)) {
            foreach (static::$order as $col=>$val) {
                $outp .= "`".static::EntityName()."`.`$col` $val, ";
            }
        }
        return trim($outp, ', ');
    }
    public function GetIndex()
    {
        return static::Index();
    }
    public static function Index()
    {
        return static::$index;
    }
    /**
     * 
     * @return Entity
     */
    public function GetRelation()
    {
        return $this->Relation();
    }
    /**
     * 
     * @return Entity
     */
    public static function Relation()
    {
        return (is_a(static::$relation, '\SMG\ORM\Entity')) ? static::$relation : false;
    }
    public function SetRelation(Entity $relation)
    {
        static::$relation = $relation;
        $this->{$relation->GetEntityName()} = $relation;
    }
    public function SetJoinRelation($key, Entity $relation)
    {
        if (isset($this->joins[$key])) {
            $this->joins[$key]->SetRelation($relation);
        }
    }
    public function SetOn(Entity $on)
    {
        static::$on = get_class($on);
        $this->{$on->GetEntityName()} = $on;
    }
    public function SetJoinOn($key, Entity $on)
    {
        if (isset($this->joins[$key])) {
            $this->joins[$key]->SetOn($on);
        }
    }
    public function GetCols()
    {
        return $this->cols;
    }
    public function SetCol($entity, $col)
    {
        if (!isset($this->cols[$entity])) {
            $this->cols[$entity] = [];
        }
        $this->cols[$entity][] = $col;
        return $this;
    }
    public function GetJoins()
    {
        return $this->joins;
    }
    public function GetOn()
    {
        return $this->On();
    }
    public static function On()
    {
        return (isset(static::$on)) ? static::$on : false;
    }
    public function GetCascade()
    {
        return $this->Cascade();
    }
    public static function Cascade()
    {
        return (isset(static::$cascade)) ? static::$cascade : false;
    }
    public function GetExisting()
    {
        return $this->existing;
    }
    public function GetCollection()
    {
        return $this->Collection;
    }
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
        // set initial values \\
        if (!isset($this->existing[$name])) {
            $this->existing[$name] = $value;
        }
    }
    public function __get($name)
    {
        return (isset($this->data[$name])) ? $this->data[$name] : null;
    }
    public function __isset($name)
    {
        return (isset($this->data[$name]));
    }
    public function __unset($name)
    {
        if (isset($this->data[$name])) {
            unset($this->data[$name]);
        }
    }
    /**
     * 
     * @param string $name
     * @param mixed $key
     * @param mixed $val
     */
    public function Append($name, $key, $val='')
    {
        if (isset($this->data[$name]) && is_array($this->data[$name])) {
            if (empty($val)) {
                $this->data[$name][] = $key;
            } else {
                $this->data[$name][$key] = $val;
            }
        }
    }
    public function FindOneBy($vals)
    {
        $this->DB->FindOneBy($vals);
        return $this;
    }
    public function FindBy($vals, $limit=0, $return=false)
    {
        $results = $this->DB->FindBy($vals, $limit);
        if (!$return) {
            $this->Collection = $results;
        }
        return $results;
    }
    public function Find($id)
    {
        $this->DB->Find($id);
        return $this;
    }
    public function Save($cascade=false)
    {
        $this->DB->Save($cascade);
    }
    public function AddEntity(Entity $entity)
    {
        // need to add this to the join so it exists for future queries, and get the data to append to it manually now \\
        $this->joins[$entity->GetEntityName()] = $entity;
        $this->cols[$entity->GetRelation()->GetEntityName()] = $entity->GetRelation()->GetCols()[$entity->GetRelation()->GetEntityName()];
        
        
        // set temporary cols and joins \\
        $tmpJoins = [$entity->GetEntityName()=>$entity];
        $tmpCols = [
            $this->GetEntityName()=>$this->GetCols()[$this->GetEntityName()],
            $entity->GetRelation()->GetEntityName()=>$entity->GetRelation()->GetCols()[$entity->GetRelation()->GetEntityName()]
        ];
        $oldJoins = $this->joins;
        $oldCols = $this->cols;
        $this->joins = $tmpJoins;
        $this->cols = $tmpCols;
        $this->FindBy([$this->GetEntityName()=>['columns'=>[$this->GetIndex()=>$this->{$this->GetIndex()}]]], 0, true);
        $this->joins = $oldJoins;
        $this->cols = $oldCols;
    }
    public function ParseJSON($entity='')
    {
        $collection = [];
        if (empty($entity)) {
            if (isset($this->Collection)) {
                $collection = $this->Collection;
            } else {
                $collection = [(int)$this->{$this->GetIndex()}=>$this->data];
            }
        } else if (isset($this->{$entity})) {
            if (is_array($this->{$entity})) {
                $collection = $this->{$entity};
            } else {
                $collection = [(int)$this->{$this->GetIndex()}=>$this->{$entity}];
            }
        }
        return $this->PrepJSON($collection);
    }
    private function PrepJSON($collection)
    {
        $json = [];
        foreach ($collection as $id=>$item) {
            if (is_object($item) && method_exists($item, 'GetRelation') && $item->GetRelation()) {
                foreach ($item->data as $col=>$val) {
                    if (!isset($item->hidden[$item->GetEntityName()]) || array_search($col, $item->hidden[$item->GetEntityName()]) === false) {
                        if (is_a($val, '\SMG\ORM\Entity')) {
                            $item->data[$col] = $this->PrepJSON([$val->{$val->GetIndex()}=>$val]);
                        } else if (is_array($val)) {
                            $item->data[$col] = $this->PrepJSON($val);
                        }
                    } else {
                        unset($item->data[$col]);
                    }
                }
                $json[$id] = $item->data;
            } else if (is_array($item)) {
                $json[$id] = $this->PrepJSON($item);
            } else {
                $json[$id] = $item;
            }
        }
        if (count($json) === 1) {
            return reset($json);
        }
        return $json;
    }
}
